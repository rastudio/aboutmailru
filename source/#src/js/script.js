@@include('jquery.spincrement.min.js');


// Dropdown vk lists

document.addEventListener("click", (e) => {
  if (e.target.classList.contains("vk-list__name")) {
    e.target.closest(".vk-list").classList.toggle("active");
  }
});

// Animation

document.addEventListener("DOMContentLoaded", () => {
  const scrollItems = document.querySelectorAll(".scroll-item");

  const scrollAnimation = () => {
    scrollItems.forEach((el) => {
      let elementPosY = document.documentElement.clientHeight - el.offsetHeight;
      if (
        el.getBoundingClientRect().top < elementPosY ||
        el.getBoundingClientRect().top <
          document.documentElement.clientHeight / 1.5
      ) {
        el.classList.add("animation-class");
      } else {
        el.classList.remove("animation-class");
      }
    });
  };

  // Animated percents

  // let percents = document.querySelectorAll("[data-percent] span");

  // percents.forEach((item) => {
  //   item.textContent = "0";
  // });

  // const scrollPercentAnimation = () => {
  //   percents.forEach((el) => {
  //     if (el.closest("div").dataset.animated == 0) {
  //       let elementPosY =
  //         document.documentElement.clientHeight - el.offsetHeight;
  //       let endValue = Number(el.closest("div").dataset.percent);
  //       if (
  //         el.getBoundingClientRect().top < elementPosY &&
  //         endValue > Number(el.textContent)
  //       ) {
  //         let currentValue = 0;
  //         let percentInterval = setInterval(function () {
  //           currentValue = currentValue + endValue / 40;
  //           if (currentValue < endValue) {
  //             // el.textContent = currentValue.toFixed(2);
  //             el.textContent = Math.round(currentValue);
  //           } else {
  //             setInterval(() => {
  //               el.textContent = endValue;
  //               el.closest("div").dataset.animated = 1;
  //               clearInterval(percentInterval);
  //             }, 1000 / 60);
  //           }
  //         }, 1000 / 60);
  //       }
  //     }
  //   });
  // };

  scrollAnimation();
  // scrollPercentAnimation();
  window.addEventListener("scroll", () => {
    scrollAnimation();
    // scrollPercentAnimation();
  });
});

$(document).ready(function () {
  let show = true;
  let countbox = ".profit__more-items";
  $(window).on("scroll load resize", function () {
    if (!show) return false; // Отменяем показ анимации, если она уже была выполнена
    let w_top = $(window).scrollTop(); // Количество пикселей на которое была прокручена страница
    let e_top = $(countbox).offset().top; // Расстояние от блока со счетчиками до верха всего документа
    let w_height = $(window).height(); // Высота окна браузера
    let d_height = $(document).height(); // Высота всего документа
    let e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками
    if (
      w_top + 500 >= e_top ||
      w_height + w_top == d_height ||
      e_height + e_top < w_height
    ) {
      $(".profit__more-items [data-percent]").css("opacity", "1");
      $(".profit__more-items [data-percent] span").spincrement({
        duration: 2000,
      });

      show = false;
    }
  });
});

$(document).ready(function () {
  let show = true;
  let countbox = ".joint__items";
  $(window).on("scroll load resize", function () {
    if (!show) return false; // Отменяем показ анимации, если она уже была выполнена
    let w_top = $(window).scrollTop(); // Количество пикселей на которое была прокручена страница
    let e_top = $(countbox).offset().top; // Расстояние от блока со счетчиками до верха всего документа
    let w_height = $(window).height(); // Высота окна браузера
    let d_height = $(document).height(); // Высота всего документа
    let e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками
    if (
      w_top + 500 >= e_top ||
      w_height + w_top == d_height ||
      e_height + e_top < w_height
    ) {
      $(".joint__items [data-percent]").css("opacity", "1");
      $(".joint__items [data-percent] span").spincrement({
        duration: 2000,
      });

      show = false;
    }
  });
});

$(document).ready(function () {
  let show = true;
  let countbox = ".statistics__wrapper";
  $(window).on("scroll load resize", function () {
    if (!show) return false; // Отменяем показ анимации, если она уже была выполнена
    let w_top = $(window).scrollTop(); // Количество пикселей на которое была прокручена страница
    let e_top = $(countbox).offset().top; // Расстояние от блока со счетчиками до верха всего документа
    let w_height = $(window).height(); // Высота окна браузера
    let d_height = $(document).height(); // Высота всего документа
    let e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками
    if (
      w_top + 500 >= e_top ||
      w_height + w_top == d_height ||
      e_height + e_top < w_height
    ) {
      $(".statistics__wrapper [data-percent]").css("opacity", "1");
      $(".statistics__wrapper [data-percent] span").spincrement({
        duration: 2000,
      });

      show = false;
    }
  });
});
